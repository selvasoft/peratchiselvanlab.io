+++
date = "2018-05-05"
title = "Hello World!"
+++
Hello people,
	For the past month, I have been thinking about starting a blog.
I wanted to have full customization. So, I didn't chose wordpress or blogger or any other corporate website creator.
Remaining option is Static Site Generator. I searched for the best SSG.
I seen <a href="https://jekyllrb.com/" target="_blank">Jekyll</a>. But, 
<a href="https://gohugo.io/" target="_blank">Hugo</a> seems to have more customization like JSON output,RSS feed etc.
Which are needed because I planned to create an app for this blog.
So, lets start this wonderful Journey.
And thanks <a href="https://gitlab.com/" target="_blank">Gitlab</a> for providing hosting free for this blog.
<span class='quora-content-embed' data-name='How-do-I-earn-money-by-developing-a-simple-static-Android-app-with-AdMmob-and-any-simple-functionality/answer/Selva-119'>Read <a class='quora-content-link' data-width='560' data-height='1000' href='https://www.quora.com/How-do-I-earn-money-by-developing-a-simple-static-Android-app-with-AdMmob-and-any-simple-functionality/answer/Selva-119' data-type='answer' data-id='98268229' data-key='42a528144b811d435127a92668f313e8' load-full-answer='False' data-embed='aormthw'><a href='https://www.quora.com/Selva-119'>Selva</a>&#039;s <a href='/How-do-I-earn-money-by-developing-a-simple-static-Android-app-with-AdMmob-and-any-simple-functionality#ans98268229'>answer</a> to <a href='/How-do-I-earn-money-by-developing-a-simple-static-Android-app-with-AdMmob-and-any-simple-functionality' ref='canonical'><span class="rendered_qtext">How do I earn money by developing a simple static Android app with AdMmob and any simple functionality?</span></a></a> on <a href='https://www.quora.com'>Quora</a><script type="text/javascript" src="https://www.quora.com/widgets/content"></script></span>